package com.spaskin.news.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Alexanrd Spaskin
 */
@Entity
@Table(name = "news")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class News {

    @Id
    @Column(name = "id_news")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer newsId;

    @Column(name = "title")
    private String title;

    @Column(name = "content", columnDefinition="TEXT")
    private String content;

    @Column(name = "create_at", columnDefinition="TIMESTAMP")
    private LocalDateTime createAt;

    @Column(name = "id_author")
    private String idAuthor;

    @Column(name = "link_a_source")
    private String linkSource;

    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "id_news")
    private List<Comment> comments;

    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "id_news")
    private List<Image> images;

    @JoinColumn(name = "image_link")
    private String imageLink;

    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JoinColumn(name = "id_news", updatable = false)
    private List<Grade> grades;

}
