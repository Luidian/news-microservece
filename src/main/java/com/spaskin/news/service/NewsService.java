package com.spaskin.news.service;

import com.spaskin.news.entity.Grade;
import com.spaskin.news.entity.News;
import com.spaskin.news.entity.Comment;
import com.spaskin.news.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;


/**
 * @author Alexanrd Spaskin
 */
@Service
public class NewsService {
    @Autowired
    private NewsRepository newsRepository;

    public void create(News news) {
        newsRepository.save(news);
    }

    public Page<News> readAll(int limit, int offset, String sortBy) {
        Pageable paging = PageRequest.of(limit, offset, Sort.by(sortBy).descending());

        return newsRepository.findAll(paging);
    }

    public News read(int id) {
        try {
            News news = newsRepository.findById(id).get();
            return news;
        } catch (NoSuchElementException e){
            return null;
        }

    }

    public boolean update(News news, int id) {
        if (newsRepository.existsById(id)){
            news.setNewsId(id);
            newsRepository.save(news);
            return true;
        }
        return false;
    }

    public boolean delete(int id) {
        if (newsRepository.existsById(id)){
            newsRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean isExist(String newsTitle){
        List<News> newsList = newsRepository.findAll();
        for (News news: newsList){
            if (news.getTitle().equals(newsTitle)){
                return true;
            }
        }
        return false;
    }

    public int grade(int id){
        News news = newsRepository.findById(id).get();
        int gradeNews = 0;
        for (Grade grade : news.getGrades()){
            if (grade.isGrade()){
                gradeNews += 1;
            } else {
                gradeNews -= 1;
            }
        }
        return gradeNews;
    }

    public List<Comment> comments(int id){
        return newsRepository.findById(id).get().getComments();
    }

}
