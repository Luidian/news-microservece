package com.spaskin.news.controllers;

import com.spaskin.news.entity.Grade;
import com.spaskin.news.entity.News;
import com.spaskin.news.entity.Comment;
import com.spaskin.news.primarykeys.GradeKey;
import com.spaskin.news.repository.NewsRepository;
import com.spaskin.news.service.GradeService;
import com.spaskin.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Alexanrd Spaskin
 */
@RestController
@RequestMapping("api/v1/news")
public class NewsController {

    private final NewsService newsService;
    private final GradeService gradeService;

    @Autowired
    NewsRepository newsRepository;
    @Autowired
    public NewsController(NewsService newsService, GradeService gradeService){
        this.newsService = newsService;
        this.gradeService = gradeService;
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody News news){
        newsService.create(news);
        return new ResponseEntity<>(news, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<News>> getAllEmployees(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(defaultValue = "createAt") String sortBy)
    {
        Page<News> list = newsService.readAll(page, size, sortBy);

        return new ResponseEntity<Page<News>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<News> read(@PathVariable(name = "id") int id){
        final News news = newsService.read(id);

        return news != null
                ? new ResponseEntity<>(news, HttpStatus.OK)
                : new ResponseEntity<>(new News(), HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<News> update(@PathVariable(name = "id") int id, @RequestBody News news){
        final boolean update = newsService.update(news, id);
        final News updateNews = newsService.read(id);

        return update
                ? new ResponseEntity<>(updateNews, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<News> delete(@PathVariable(name = "id") int id){
        final News deletedNews = newsService.read(id);
        final boolean deleted = newsService.delete(id);

        return deleted
                ? new ResponseEntity<>(deletedNews, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @PostMapping("/grade")
    public ResponseEntity<?> createGrade(@RequestBody Grade grade){
        gradeService.create(grade);

        return new ResponseEntity<>(grade, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}/grade")
    public ResponseEntity<Integer> readGrade(@PathVariable(name = "id") int id){
        final int size = newsService.grade(id);

        return new ResponseEntity<>(size, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/comments")
    public ResponseEntity<List<Comment>> readComments(@PathVariable(name = "id") int id){
        final List<Comment> comments = newsService.comments(id);

        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{idNews}/grade/{idUser}")
    public ResponseEntity<Grade> gradeDelete(@PathVariable(name = "idNews") int idNews, @PathVariable(name = "idUser") String idUser){
        final Grade deletedGrade = gradeService.read(new GradeKey(idNews, idUser));
        final boolean deleted = gradeService.delete(new GradeKey(idNews, idUser));

        return deleted
                ? new ResponseEntity<>(deletedGrade, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
