package com.spaskin.news.parser;

import com.spaskin.news.entity.News;
import com.spaskin.news.service.NewsService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

/**
 * @author Alexanrd Spaskin
 */
@Component
public class RSSNewsParser {

    static final String TITLE = "title";
    static final String LINK = "link";
    static final String ITEM = "item";

    @Autowired
    NewsService newsService;

    final URL url = new URL("https://www.sport.ru/rssfeeds/news.rss");
    String eventReaderLastName = "";

    public RSSNewsParser() throws MalformedURLException {
    }

    @Scheduled(fixedDelay = 60, timeUnit = TimeUnit.MINUTES)
    public void readFeed() {
        List<News> newsList = new ArrayList<>();
        try {
            String title = "";
            String link = "";

            XMLInputFactory inputFactory = XMLInputFactory.newInstance();

            InputStream in = read();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            if (eventReader.toString().equals(this.eventReaderLastName)){
                return;
            }
            this.eventReaderLastName = eventReader.toString();

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    String localPart = event.asStartElement().getName()
                            .getLocalPart();
                    switch (localPart) {
                        case TITLE:
                            title = getCharacterData(event, eventReader);
                            break;
                        case LINK:
                            link = getCharacterData(event, eventReader);
                            break;
                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
                        News news = new News();
                        news.setTitle(title);
                        news.setLinkSource(link);
                        Document siteDoc = parseSiteInDoc(link);
                        if (siteDoc == null){
                            continue;
                        }
                        String content  = getContentFromSite(siteDoc);
                        if (content.equals("")){
                            continue;
                        }
                        news.setContent(content);
                        String imageLink = getImageLinkFromSite(siteDoc);
                        news.setImageLink(imageLink);
                        news.setCreateAt(LocalDateTime.now());
                        newsList.add(news);
                        event = eventReader.nextEvent();
                        continue;
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
        addNewsDB(newsList);
    }

    private void addNewsDB(List<News> newsList){
        Collections.reverse(newsList);
        for (News news: newsList) {
            if (!newsService.isExist(news.getTitle())){
                newsService.create(news);
            }
        }
    }

    private String getCharacterData(XMLEvent event, XMLEventReader eventReader)
            throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }

    private InputStream read() {
        try {
            return url.openStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Document parseSiteInDoc(String url){
        try {
            Document doc = Jsoup.connect(url)
                    .userAgent("Mozilla")
                    .timeout(5000)
                    .referrer("https://google.com")
                    .get();
            return doc;
        } catch (IOException e) {
            return null;
        }
    }

    private String getContentFromSite(Document doc){
        String content = doc.select(".article-text").text();
        if (content.equals("Источник: Sport.ru")){
            return "";
        }
        return content;
    }

    private String getImageLinkFromSite(Document doc){
        Element img = doc.select(".article-image-large img").first();
        String imageLink = img.absUrl("src");
        return imageLink;
    }
}
